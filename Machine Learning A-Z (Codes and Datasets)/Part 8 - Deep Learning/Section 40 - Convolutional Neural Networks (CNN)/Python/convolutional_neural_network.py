#!/usr/bin/env python
# coding: utf-8

# # Convolutional Neural Network

# ### Importing the libraries

# In[1]:


import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator # this is compulsory when pre processing images


# In[2]:


tf.__version__


# In[3]:


# the following line it is used to check if we are using the CPU or the GPU for the computatio of tensorflo
# if it print something like "device_type='GPU'" we are using the GPU (recommended) otherwise we are using the CPU
# (not recommended, expecialli for old CPUs)
print(tf.config.list_physical_devices('GPU'))


# ## Part 1 - Data Preprocessing

# ### Preprocessing the Training set

# In[4]:


# we want to apply some transformation on the images of the Training-Set to avoid overfitting

# if we don't apply these transformation, when we will train the CNN on the Test-Set we will get very high
# accuracies on the Training-Set and much lower on the Test-Set, this is the overfitting

# these transformations are geometrical transformations like: zooms, rotations or shift some pixels
# the technical term of what we are going to do now with all these transformations is called image augmentation,
# because we augment the variety and diversity of the Training-Set images

# ImageDataGenerator is an instance of the ImageDataGenerator class, we use this to apply all the transformations
# of the Training-Set:
#    rescale: apply feature scaling to each pixel by dividing their value, in this case, by 255, because each
#             pixel take a value between 0 and 255, so we get all the pixel values between 0 and 1, is like a
#             normalization, feature scaling is compulsory for neural network
#    shear_range: to apply shearing transformations
#    zoom_range: to zoom
#    horizontal_flip: to horizontally flip each image
train_datagen = ImageDataGenerator( 
    rescale = 1./255, 
    shear_range = 0.2,
    zoom_range = 0.2,
    horizontal_flip = True
)


# now we need to connect the object just created to transform the train images to the Training-Set:
#    the first argument is the path to the Training-Set
#    target_size: the final size of our images when they will be fed into the convolutional neural network,
#                 we use (64, 64) to make the training not so slow and still have good results
#    batch_size: the size of each batch, the group of images processed before update the weights, 32 is a classic value
#    class_mode: it could be binary or categorical, in this case we can have just 2 results so we choose binary
training_set = train_datagen.flow_from_directory(
    'dataset/training_set',
    target_size = (64, 64),
    batch_size = 32,
    class_mode = 'binary'
)


# ### Preprocessing the Test set

# In[5]:


# for the Test-Set we just apply the scaling, it must be the same scaling used for the Training-Set, because the
# neural network need it

# we do not apply others scaling, because this set represents the real images, like the images
# that will be used in production and we want to keep them intact like the original ones
test_datagen = ImageDataGenerator(rescale = 1./255)

test_set = test_datagen.flow_from_directory(
    'dataset/test_set',
    target_size = (64, 64),
    batch_size = 32,
    class_mode = 'binary'
)


# ## Part 2 - Building the CNN

# ### Initialising the CNN

# In[6]:


# we use tf.keras.models.Sequential to create a neural network as a sequence of layers
cnn = tf.keras.models.Sequential()


# ### Step 1 - Convolution

# In[7]:


# tf.keras.layers.Conv2D is the function to create the convolutional layer, it takes these parameters:
#    filters: number of feature detector we want to apply to our images (also called "kernels"), it's the number of
#             output filter in the vonvolution, 32 is a classic value for the first and second convolutional layer
#    kernel_size: the size of the feature detector, we specify the number of rows, the number of columns is implicit
#                 because usually it is a squared array
#    activation: the activation function, we use the rectifier activation function ("relu")
#    input_shape: when we add our very first layer, whether it is a convolutional layer or a dense layer, we need
#                 to specify the input shape of our inputs, since we are working with color images of the size of 64x64
#                 we use [64, 64, 3] because these images are in 3 dimension, these dimensions are the RGB code of colors,
#                 if we were working with black and white images we would have 1 instead of 3
cnn.add(tf.keras.layers.Conv2D(
    filters=32, 
    kernel_size=3, 
    activation='relu', 
    input_shape=[64, 64, 3]
))


# ### Step 2 - Pooling

# In[8]:


# we use tf.keras.layers.MaxPool2D to apply Pooling, more exactly Max Pooling, the parameters are:
#    pool_size: the size of the frame that we apply on each Feature Map to search the maximum value to
#               insert in the Pooled Feature Map, we specify the number of rows, the number of columns 
#               is implicit because usually it is a squared array, 2 is a recommended value
#    strides: it indicates the number of pixels that by which the frame shift to the right, we choose the same
#             size of the frame
cnn.add(tf.keras.layers.MaxPool2D(
    pool_size=2, 
    strides=2
))


# ### Adding a second convolutional layer

# In[9]:


# we apply the second convolutional layer on the result of the pooling, this layer is equal to the 
# first one, except for the input_shape because we don't need it anymore
cnn.add(tf.keras.layers.Conv2D(
    filters=32, 
    kernel_size=3, 
    activation='relu'
))

# we apply again the Max Pooling, this is also equal to the previous one
cnn.add(tf.keras.layers.MaxPool2D(
    pool_size=2, 
    strides=2
))


# ### Step 3 - Flattening

# In[10]:


# with tf.keras.layers.Flatten we flat the result of the last pooling into a 1 dimensional vector, to use it as input
# of the fully connected neural network
cnn.add(tf.keras.layers.Flatten())


# ### Step 4 - Full Connection

# In[11]:


# tf.keras.layers.Dense is the same function used in the last Python example for the Artificial Neural Network,
# we pass to it these parameters:
#    units: the number of hidden neurons in this fully connected layer, we choose a large number because this problem
#           is more complex of the last one
#    activation: as long as we haven't reach the output layer is always recommend the rectifier activation function
cnn.add(tf.keras.layers.Dense(
    units=128, 
    activation='relu'
))


# ### Step 5 - Output Layer

# In[12]:


# the output layer is still a fully connected layer, so we use tf.keras.layers.Dense but only with 1 neuron
# because we are doing a binary classification and for the activation function, since this is the output layer,
# we use the sigmoid activation function because we are doing binary classification, if we were doing
# multiclass classification we would have the softmax activation function
cnn.add(tf.keras.layers.Dense(
    units=1, 
    activation='sigmoid'
))


# ## Part 3 - Training the CNN

# ### Compiling the CNN

# In[13]:


# as the previous section, we use cnn.compile to compile our CNN, with the same parameters:
#     optimizer: the best optimizer that we have seen is the Stochastic Gradient Descent, the "adam" optimizer use 
#                the SGD and is one of the most common used
#     loss: for binary classification the loss function must always be the "binary_crossentropy", for non 
#           binary classification must be the categorical_crossentropy
#     metrics: list of metrics to evaluate the ANN, during the training, we only choose the "accuracy"
cnn.compile(
    optimizer = 'adam', 
    loss = 'binary_crossentropy', 
    metrics = ['accuracy']
)


# ### Training the CNN on the Training set and evaluating it on the Test set

# In[14]:


# finally, we fit our CNN with cnn.fit
#     x: the Training-Set to train the CNN
#     validation_data: the Test-Set for evaluate the CNN
#     epochs: the number of epochs, 25 is a good value, 10 or 15 could be too low

# we can see that each epoch contains 250 images, that is because we choose a batch size of 32 and in total in the
# Training-Set there are 8000 images, 8000/32 = 250
cnn.fit(
    x = training_set, 
    validation_data = test_set, 
    epochs = 25
)


# ### The training ended with an accuracy on:
# * Training-Set of 92% (accuracy: 0.9209) 
# * Test-Set of 77% (val_accuracy: 0.7710)

# ## Part 4 - Making a single prediction

# In[3]:


import numpy as np
from keras import utils

def import_and_evaluate_image(model, path):
    # load the image from the file system, specifying the size, to resize the image as the others images 
    # used during the training
    test_image = utils.load_img(path, target_size = (64, 64))
    
    # we convert the image in PIL format into a numpy array, that is exactly the format of array expected by the predict method
    test_image = utils.img_to_array(test_image)
    
    # the predict method must be called on the same format that we used during the training, we traing our CNN with 
    # batch of images and therefore we have an extra dimension corresponding to the batch that contains group of 32 images.
    # we add this extra dimension with the Numpy function "expand_dims" at which we pass the image and where we want
    # to add this extra dimension, the dimension of the batch is always the firs dimension that will contains all the images,
    # so we use "axis = 0"
    test_image = np.expand_dims(test_image, axis = 0)
    
    # we take the prediction from the image
    result = model.predict(test_image)
       
    # the first element correspond to the prediction
    if result[0][0] > 0.5:
        prediction = 'dog'
    else:
        prediction = 'cat'
    
    return prediction


# In[16]:


# how can we know if 0 or 1 correspond to cat or dog?
training_set.class_indices

# the labels "cats" and "dogs" correspond to the name of the folders, the choose between 0 and 1 is given by
# the order of the folders


# In[27]:


print(import_and_evaluate_image(cnn, 'dataset/single_prediction/cat_or_dog_1.jpg'))
print(import_and_evaluate_image(cnn, 'dataset/single_prediction/cat_or_dog_2.jpg'))


# In[28]:


print(import_and_evaluate_image(cnn, 'dataset/Nala/Nala1.jpg'))
print(import_and_evaluate_image(cnn, 'dataset/Nala/Nala2.jpg'))
print(import_and_evaluate_image(cnn, 'dataset/Nala/Nala3.jpg'))
print(import_and_evaluate_image(cnn, 'dataset/Nala/Nala4.jpg'))
print(import_and_evaluate_image(cnn, 'dataset/Nala/Nala5.jpg'))


# In[34]:


print(import_and_evaluate_image(cnn, 'dataset/Benji/Benji1.jpg')) # should be a dog
print(import_and_evaluate_image(cnn, 'dataset/Benji/Gatto.jpg')) # should be a cat


# ## EXTRA - Save a model for the future

# In[ ]:


# creates a HDF5 file 'cnn_saved.h5'
cnn.save('cnn_saved.h5')


# In[4]:


from keras.models import load_model

# returns a compiled model imported from a file
cnn_saved = load_model('cnn_saved.h5')

print(import_and_evaluate_image(cnn_saved, 'dataset/single_prediction/cat_or_dog_1.jpg'))

