# Fondamenti di ML

Questo progetto contiene gli appunti in LaTex e le applicazioni pratiche in Python che ho appreso dal corso di Machine Learning [Machine Learning A-Z™: Hands-On Python & R In Data Science](https://www.udemy.com/course/machinelearning/).

### Prerequisiti

- Livello di matematica da scuola superiore
- Conoscenze basilari di programmazione

## Autore

  - **Marco Cordoni**
